#include "main.h"
#include "mem_internals.h"

bool test_free() {
  size_t sz[N];
  sz[0] = 1024;
  for (size_t i = 1; i < N; ++i)
    sz[i] = sz[i - 1] * 2;
  for (size_t i = 0; i < N; ++i) {
    fprintf(stderr, "Test %zu: %zu", i, sz[i]);
    void *h = heap_init(sz[i]);

    if (!h) {
      fprintf(stderr, " failed: heap was not initialized properly\n");
      return false;
    }

    void *d1 = _malloc(LOCAL); 
    
    if (!d1) {
      fprintf(stderr, " failed: data was not allocated properly\n");
      return false;
    }
    debug_heap(stderr, h);
    _free(d1);
    debug_heap(stderr, h);
    heap_term();

    fprintf(stderr, " OK\n");
  }
  return true;
}

bool test_malloc() {
  size_t sz[N];
  sz[0] = 1024;
  for (size_t i = 1; i < N; ++i)
    sz[i] = sz[i - 1] * 2;
  for (size_t i = 0; i < N; ++i) {
    fprintf(stderr, "Test %zu: %zu", i, sz[i]);
    void *h = heap_init(sz[i]);

    if (!h) {
      fprintf(stderr, " failed: heap was not initialized properly\n");
      return false;
    }

    void *a = _malloc(REGION_MIN_SIZE);
    void *b = _malloc(LOCAL);

    if (!a || !b) {
      fprintf(stderr, " failed: data was not allocated properly\n");
      return false;
    }

    debug_heap(stderr, h);
    _free(a);
    if (a) {
      fprintf(stderr, " failed: data was not deallocated properly\n");
      return false;
    }
    _free(b);
    if (b) {
      fprintf(stderr, " failed: data was not deallocated properly\n");
      return false;
    }
    debug_heap(stderr, h);

    heap_term();

    fprintf(stderr, " OK\n");
  }
  return true;
}

bool test_small_queries() {
  for (size_t i = 0; i < N; ++i) {
    fprintf(stderr, "Test %zu", i);
    void *h = heap_init(1024);

    if (!h) {
      fprintf(stderr, " failed: heap was not initialized properly\n");
      return false;
    }

    debug_heap(stderr, h);
    void *k;
    for (size_t j = 0; j < N/M; ++j) {
      k = _malloc(LOCAL);
    }

    if (!k) {
      fprintf(stderr, " failed: data was not allocated properly\n");
      return false;
    }

    debug_heap(stderr, h);
    _free(k);
    debug_heap(stderr, h);
    heap_term();
  }
  return true;
}

int main() {
  return !test_free() || !test_small_queries() || !test_malloc();
}